const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

const jsonParser = bodyParser.json();
const slackController = require('../controllers/slackController');
const slackValidator = require('../validators/slackValidator');

router.get('/jobs', slackController.listJobs);

router.delete('/jobs/:id', slackValidator.deleteJob, slackController.deleteJob);

router.post(
  '/jobs',
  [
    jsonParser,
    slackValidator.addNewJob
  ],
  slackController.addNewJob
);

module.exports = router;
