const expect = require('chai').expect;
const rp = require('request-promise');
const { serverUrl } = require('../../../config.json');

const { PORT } = require('../../../server').PORT();

const sampleJob = {
  message: 'Test job56',
  time: '2018-07-09T04:02:03.118Z',
  channel: '#general',
  status: 'scheduled'
};
let sampleId = 0;

describe('Test slack routes', () => {
  describe('POST job', () => {
    it('should return added job message', async () => {
      const response = await rp({
        method: 'POST',
        uri: `${serverUrl}:${PORT}/API/slack/jobs`,
        body: sampleJob,
        json: true
      });
      sampleId = response.data.$loki;
      expect(response.success).to.be.true;
      expect(response.data).to.be.a('object');
      expect(response.data).to.own.include({ message: 'Test job56' });
    });
  });

  describe('GET jobs', () => {
    it('should return jobs array', async () => {
      const response = await rp({
        method: 'GET',
        uri: `${serverUrl}:${PORT}/API/slack/jobs`,
        json: true
      });
      expect(response.success).to.be.true;
      expect(response.data).to.be.a('array');
      expect(response.data[0]).to.include.all.keys(
        'message', 'time', 'channel', 'status', '$loki', 'meta');
    });
  });

  describe('DELETE job', () => {
    it('should return deleted job id', async () => {
      const response = await rp({
        method: 'DELETE',
        uri: `${serverUrl}:${PORT}/API/slack/jobs/${sampleId}`,
        json: true
      });
      expect(response.success).to.be.true;
      expect(Number(response.data)).equal(sampleId);
    });
  });
});
