module.exports = db => {
  return new Promise(function (resolve, reject) {
    db.loadDatabase({}, function (err) {
      if (err) {
        reject(err);
      } else {
        var jobs = db.getCollection('jobs');
        if (!jobs) {
          jobs = db.addCollection('jobs');
        }
        resolve(jobs);
      }
    });
  });
};
