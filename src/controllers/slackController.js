const SlackModel = require('../models/slackModel');
const Slack = new SlackModel();

exports.addNewJob = async (req, res) => {
  const addedJob = await Slack.addNewJob(req.body);
  res.json({
    success: true,
    data: addedJob
  });
};

exports.deleteJob = async (req, res) => {
  const deletedJobId = await Slack.deleteJob(req.params.id);
  res.json({
    success: true,
    data: deletedJobId
  });
};

exports.listJobs = async (req, res) => {
  const jobs = await Slack.listJobs(); // get jobs
  res.json({
    success: true,
    data: jobs
  });
};

exports.updateJobStatus = (job, status) => {
  Slack.updateJobStatus(job, status);
};
