const validator = require('validator');
// const xssFilters = require('xss-filters');
// ToDo json schema validator
exports.addNewJob = (req, res, next) => {
  if (
    req && req.body &&
    req.body.message && req.body.message.length >= 1
  ) {
    return next();
  }
  res.json({
    success: false,
    data: 'Error adding a new job'
  });
};

exports.deleteJob = (req, res, next) => {
  if (validator.isInt(req.params.id)) return next();
  res.json({
    success: false,
    data: 'Error deleting job'
  });
};
