var Loki = require('lokijs');
const path = require('path');
const schedule = require('node-schedule');
const moment = require('moment');

const LokiFSStructuredAdapter = require('lokijs/src/loki-fs-structured-adapter');

const getJobsDB = require('../storage/getJobsDB');

const postSlackMessage = require('../remoting/postSlackMessage');

// const sortByDate = (left, right) => {
//   return moment.utc(left.timeStamp).diff(moment.utc(right.timeStamp));
// };

module.exports = class Slack {
  constructor () {
    this.db = new Loki(path.join(__dirname, '../../storage/jobs.json'), {
      adapter: new LokiFSStructuredAdapter()
    });
    this.initDB = getJobsDB(this.db);
    Slack.sheduledJobs = {};
  }

  async addNewJob (data) {
    const jobs = await this.initDB;
    const resultObj = jobs.insert(data);
    Slack.scheduleJob(resultObj);
    this.db.saveDatabase();
    return resultObj;
  }

  async updateJobStatus (job, status) {
    const jobs = await this.initDB;
    job.status = status;
    jobs.update(job);
    this.db.saveDatabase();
  }

  async deleteJob (id) {
    const jobs = await this.initDB;
    jobs.remove({ $loki: id });
    Slack.cancelJob(id);
    this.db.saveDatabase();
    return id;
  }

  async listJobs () {
    const jobs = await this.initDB;
    const allJobs = jobs.addDynamicView('allJobs');
    await allJobs.applySimpleSort('$loki');
    return allJobs.data();
  }

  static scheduleJob (job) {
    const { $loki, time } = job;
    const y = moment(time).get('year');
    const m = moment(time).get('month'); // 0 to 11
    const d = moment(time).get('date');
    const h = moment(time).get('hour');
    const mi = moment(time).get('minute');
    const s = moment(time).get('second');
    const date = new Date(y, m, d, h, mi, s);
    Slack.sheduledJobs[$loki] = schedule.scheduleJob(date, () => {
      postSlackMessage(job);
    });
  }

  static cancelJob (jobId) {
    if (Slack.sheduledJobs[jobId]) {
      Slack.sheduledJobs[jobId].cancel();
    }
  }
};
