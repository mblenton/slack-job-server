const rp = require('request-promise');

const { SLACK_WEBHOOK_URL } = require('../../server').SLACK_WEBHOOK_URL();
const slackController = require('../controllers/slackController');

module.exports = async job => {
  const { message } = job;
  try {
    const response = await rp({
      method: 'POST',
      uri: SLACK_WEBHOOK_URL,
      json: true,
      body: {
        text: message
      }
    });
    if (response === 'ok') {
      slackController.updateJobStatus(job, 'sent');
    } else {
      slackController.updateJobStatus(job, response.toString());
    }
    return response;
  } catch (error) {
    slackController.updateJobStatus(job, error.toString());
    throw error;
  }
};
