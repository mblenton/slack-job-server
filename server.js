const compression = require('compression');
const express = require('express');
const app = express();
const cors = require('cors');
const path = require('path');

const { corsOrigins, slackWebhookURL } = require('./config.json');

const PORT = process.env.PORT || 3030;
const SLACK_WEBHOOK_URL = process.env.SLACK_WEBHOOK_URL || slackWebhookURL;

exports.PORT = () => ({ PORT });
exports.SLACK_WEBHOOK_URL = () => ({ SLACK_WEBHOOK_URL });

corsOrigins.push(`http://localhost:${PORT}`);

const corsOptions = {
  origin: function (origin, callback) {
    if (corsOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  }
};

const slackRoutes = require('./src/routes/slackRoutes');

app.use(compression());

app.use(express.static(path.join(__dirname, '/public')));

app.use('/API/slack', cors(corsOptions), slackRoutes);

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

app.use((req, res) => {
  res
    .status(404)
    .send('Bad Request');
});

app.use((err, req, res, next) => {
  res
    .status(500)
    .send('Something broke!');
  if (err) {}
});

app.listen(PORT);

console.log('todo list RESTful API server started on: ' + PORT);
